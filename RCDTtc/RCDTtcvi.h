#ifndef RCDTTCVI_H
#define RCDTTCVI_H

//******************************************************************************
// file: RCDTtcvi.h
// desc: library for TTCvi module
// auth: 02/05/00 R. Spiwoks
// modf: 31/10/01 R. Spiwoks, new VMEbus API
// modf: 05/12/02 R. Spiwoks, DataFlow repository
// modf: 06/12/02 R. Spiwoks, new user interface
// modf: 31/01/03 R. Spiwoks, distinction MkI/MkII, clean-up
// modf: 24/07/03 M. Gruwe, fix bug: MAX_CSR1_RND = 0x7000
// modf: 23/01/04 M. Gruwe: Fixup (specify std:: when necessary)
// modf: 18/06/05 L. Levinson: fix for calibration mode
// modf: 04/11/05 R. Spiwoks:, modified constants RNDM_xxxx to be [0..7]
//******************************************************************************

#include <string>

#include "RCDVme/RCDVme.h"

namespace RCD {

// TTCvi class -----------------------------------------------------------------

class TTCVI {

  public:

    // data types --------------------------------------------------------------
    typedef struct {
	u_short			address;
        bool			external;
	u_char			sub_address;
	u_char			data;
    } LongCommand;
    typedef u_char ShortCommand;

    // general methods ---------------------------------------------------------

    // global methods
    TTCVI(u_int );
   ~TTCVI();
    u_int reset(bool);
    u_int dump();

    // configuration/identification
    u_int manufacturerGet(u_int* );
    u_int boardIdentifierGet(u_int* );
    u_int boardRevisionGet(u_int* );
    u_int mkTypeGet(int* );
    static const int		MK_TYP1		=      1;
    static const int		MK_TYP2		=      2;

    // BC and Orbit methods
    u_int bcDelayGet(int* );
    u_int orbitInputSet(u_short );
    u_int orbitInputGet(u_short* );
    static const u_short	ORB_INT		= 0x0008;
    static const u_short	ORB_EXT		= 0x0000;

    // trigger word methods
    u_int triggerWordEnable(LongCommand& );
    u_int triggerWordDisable( );
    u_int triggerWordGet(LongCommand*, bool* );

    // Event/Orbit counter
    u_int counterValueGet(int* );
    u_int counterSelectionSet(u_short );
    u_int counterSelectionGet(u_short* );
    static const u_short	CNT_ORB		= 0x8000;
    static const u_short	CNT_L1A		= 0x0000;
    u_int counterReset();

    // Level-1 Accepts (L1As) --------------------------------------------------

    // L1A generation - input selection
    u_int l1aInputSet(u_short );
    u_int l1aInputGet(u_short* );
    static const u_short	L1A_EXT0	= 0x0000;
    static const u_short	L1A_EXT1	= 0x0001;
    static const u_short	L1A_EXT2	= 0x0002;
    static const u_short	L1A_EXT3	= 0x0003;
    static const u_short	L1A_VME		= 0x0004;
    static const u_short	L1A_RNDM	= 0x0005;
    static const u_short	L1A_CALIB	= 0x0006;
    static const u_short	L1A_DISABLE	= 0x0007;
    static const std::string	L1A_NAME[8];

    // L1A generation - random generator
    u_int l1aRandomSet(u_short );
    u_int l1aRandomGet(u_short* );
    static const u_short	RNDM_1HZ	= 0x0000;
    static const u_short	RNDM_100HZ	= 0x0001;
    static const u_short	RNDM_1KHZ	= 0x0002;
    static const u_short	RNDM_5KHZ	= 0x0003;
    static const u_short	RNDM_10KHZ	= 0x0004;
    static const u_short	RNDM_25KHZ	= 0x0005;
    static const u_short	RNDM_50KHZ	= 0x0006;
    static const u_short	RNDM_100KHZ	= 0x0007;
    static const std::string	RNDM_NAME[8];

    // L1A generation - VMEbus request
    u_int l1aGenerate();

    // L1A FIFO
    u_int l1aFifoEmpty(bool* );
    u_int l1aFifoFull(bool* );
    u_int l1aFifoReset();

    // B-Channel commands ------------------------------------------------------

    // number of BGO registers
    static const   int		BGO_NUM	 	=      4;

    // B-Go Channels - mode selection
    u_int bgoModeSet(int, u_short);
    u_int bgoModeGet(int, u_short* );
    static const u_short	BGO_ENABLE	= 0x0001;
    static const u_short	BGO_SYNC	= 0x0002;
    static const u_short	BGO_SINGLE	= 0x0004;
    static const u_short	BGO_FIFO	= 0x0008;
    static const u_short	BGO_CALIB	= 0x0010;

    // B-Go channels - data writing
    u_int bgoCommandPut(int, LongCommand& );
    u_int bgoCommandPut(int, ShortCommand& );
    u_int bgoCommandPut(int, u_int command );

    // B-Go channels - VMEbus request
    u_int bgoGenerate(int);

    // B-Go inhibt signals
    u_int bgoInhibitOn(int, u_short, u_short);
    u_int bgoInhibitOff(int chn);
    u_int bgoInhibitGet(int, u_short*, u_short* );

    // B-Go FIFOs
    u_int bgoFifoEmpty(int, bool* );
    u_int bgoFifoFull(int, bool* );
    u_int bgoFifoRetransSet(int, bool);
    u_int bgoFifoRetransGet(int, bool* );
    u_int bgoFifoReset(int);

    // asynchronous commands
    u_int asyncCommand(ShortCommand& );
    u_int asyncCommand(LongCommand& );
    u_int asyncPendingGet(bool* );

    // operator to return status of object -------------------------------------
    u_int operator()()const		{ return(m_status); };

    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------

  private:

    // -- conf/id EEPROM register addresses
    static const u_int		REG_MANUF_ID	= 0x0026;
    static const u_int		REG_BOARD_ID	= 0x0032;
    static const u_int		REG_BOARD_RV	= 0x0042;

    // -- CSR register addresses
    static const u_int		REG_CSR1	= 0x0080;
    static const u_int		REG_CSR2	= 0x0082;
    static const u_int		REG_RESET	= 0x0084;

    // -- CSR1 registers: BC, ORBIT and VME
    static const u_short	MSK_CSR1_BC	= 0x0f00;
    static const int		SHF_CSR1_BC	=      8;
    static const int		CNV_CSR1_BC	=      2;
    static const u_short	MSK_CSR1_ORB	= 0x0008;
    static const u_short	MSK_CSR1_VME	= 0x0080;

    // -- L1A registers: generate and counter
    static const u_int		REG_L1A		= 0x0086;
    static const u_int		REG_CNT_HI	= 0x0088;
    static const u_int		REG_CNT_LO	= 0x008a;
    static const u_int		REG_CNT_RST	= 0x008c;
    static const u_short	MSK_CNT_LO	= 0xffff;
    static const u_short	MSK_CNT_HI	= 0x00ff;
    static const int 		SHF_CNT_HI	=     16;
    static const u_short	MSK_CSR1_L1A	= 0x0007;
    static const u_short	MAX_CSR1_L1A	= 0x0007;  // LL 18-Jun-04
    static const u_short	MSK_CSR1_RND	= 0x7000;
    static const u_short	MAX_CSR1_RND	= 0x0007;
    static const int		SHF_CSR1_RND	=     12;
    static const u_short	MSK_CSR1_FUL	= 0x0010;
    static const u_short	MSK_CSR1_EMP	= 0x0020;
    static const u_short	MSK_CSR1_RST	= 0x0040;
    static const u_short	MSK_CSR1_CNT	= 0x8000;

    // -- trigger word addresses
    static const u_int		REG_TWRD_HI	= 0x00c8;
    static const u_int		REG_TWRD_LO	= 0x00ca;
    static const u_int		MSK_TWRD_ADDR	= 0x3fff;
    static const u_int		MSK_TWRD_SADD	= 0x00fc;
    static const u_short	BIT_TWRD_EXT	= 0x0100;
    static const u_short	BIT_TWRD_SIZE	= 0x0200;

    // -- BGO registers: generate and mode
    static const u_int		REG_BGO		= 0x0096;
    static const u_int		REG_BMOD	= 0x0090;
    static const u_int		STP_BGO		= 0x0008;
    static const u_short 	MSK_BMOD	= 0x000f;
    static const u_short 	MAX_BMOD	= 0x001f;
    static const u_short 	BIT_CSR2_FIFO	= 0x0001;
    static const u_short 	BIT_CSR2_RTRS	= 0x0100;
    static const u_short 	MSK_CSR2_RTRS	= 0x0f00;
    static const u_short 	BIT_CSR2_RSET	= 0x1000;

    // -- BGO data registers
    static const u_int		REG_BDAT	= 0x00b0;
    static const u_int		STP_BDAT	= 0x0004;
    static const u_int		MSK_BDAT_SHORT	= 0x00ff;
    static const int		SHF_BDAT_SHORT	=     23;
    static const u_int		BIT_BDAT_LONG	= 0x80000000;
    static const u_int		BIT_BDAT_EXT	= 0x00010000;
    static const u_int		MSK_BDAT_ADDR	= 0x3fff;
    static const int		SHF_BDAT_ADDR	=     17;
    static const u_int		MSK_BDAT_DATA	= 0x00ff;
    static const int		SHF_BDAT_SUBA	=      8;

    // -- BGO Inhibit registers: delay and duration
    static const u_int		REG_IDEL	= 0x0092;
    static const u_short	MSK_IDEL	= 0x0fff;
    static const u_int		REG_IDUR	= 0x0094;
    static const u_short	MSK_IDUR	= 0x00ff;
    static const int		CNV_INH		=     25;

    // -- asynchronous B channel commands
    static const u_int		REG_SHORT	= 0x00c4;
    static const u_short	MSK_SHORT_DATA	= 0x00ff;
    static const u_int		REG_LONG_HI	= 0x00c0;
    static const u_int		REG_LONG_LO	= 0x00c2;
    static const u_short	BIT_LONG_MARK	= 0x8000;
    static const u_short	BIT_LONG_EXT	= 0x0001;
    static const int		STP_LONG_ADDR	=      1;

    // -- VMEbus driver/library and master mapping
    VME*			m_vme;
    VMEMasterMap*		m_vmm;
    static const u_int		VME_SIZE	= 0x0100;

    // -- general usage parameters
    int				m_type;
    u_short			m_dats;
    u_int			m_addr;
    u_int			m_status;
};

}	// namespace RCD

#endif // RCDTTCVI_H
