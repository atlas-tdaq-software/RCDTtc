///*****************************************************************************
// file: RCDTtcvi.cc
// desc: library for TTCvi module
// auth: 02/05/00 R. Spiwoks
// modf: 31/10/01 R. Spiwoks, new VMEbus API
// modf: 12/05/02 R. Spiwoks, DataFlow repository
// modf: 06/12/02 R. Spiwoks, new user interface
// modf: 31/01/03 R. Spiwoks, distinction MkI/MkII, clean-up
// modf: 28/07/03 M. Gruwe, fix bug in triggerWordGet (Enable/Disable flag was wrong)
// modf: 20/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fixup (specify std:: when necessary)
// modf: 18/06/04 L. Levinson: fix for calibration mode
///*****************************************************************************

#include <iostream>
#include <errno.h>
#include <iomanip>
#include <cstdio>

#include "DFDebug/DFDebug.h"
#include "RCDTtc/RCDTtcvi.h"

using namespace RCD;

//------------------------------------------------------------------------------

const std::string	TTCVI::RNDM_NAME[8]	=
	{"  1  Hz", "100  Hz", "  1 kHz", "  5 kHz",
	 " 10 kHz", " 25 kHz", " 50 kHz", "100 kHz"};
const std::string	TTCVI::L1A_NAME[8]	=
	{"  L1A<0>", "  L1A<1>", "  L1A<2>", "  L1A<3>",
	 "     VME", "    RNDM", "   CALIB", "DISABLED"};		   // LL 18-Jun-04

//------------------------------------------------------------------------------

TTCVI::TTCVI(u_int base) {

    // open VME library
    m_vme = VME::Open();
    if((*m_vme)()) {
	ERR_TEXT(DFDB_RCDTTC,"opening VME object");
	m_vme->ErrorPrint((*m_vme)());
    }

    // create VME master mapping
    m_vmm = m_vme->MasterMap(base,VME_SIZE,VME_A24,0);
    if((*m_vmm)()) {
	ERR_TEXT(DFDB_RCDTTC,"opening VME master mapping");
	m_vme->ErrorPrint((*m_vmm)());
    }

    // get Mk type
    ((m_status = m_vmm->ReadSafe(REG_TWRD_HI,&m_dats)) != VME_SUCCESS) ?
	m_type = MK_TYP1 : m_type = MK_TYP2;

    // check if readable
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR1");
    }

    // print open statement
    if(m_status == VME_SUCCESS) {
      DEBUG_TEXT(DFDB_RCDTTC,15,"RCD::TTCvi library opened, Mk type = " << std::dec << m_type
		 << ", phys = " << std::hex << std::setfill('0') << std::setw(8) << base << std::dec);
    } else {
      ERR_TEXT(DFDB_RCDTTC,"RCD::TTCvi library, error = " << std::dec << m_status);
    }
}

//------------------------------------------------------------------------------

TTCVI::~TTCVI(void) {

    // unmap VME master mapping
    m_vme->MasterUnmap(m_vmm);

    // close VME library
    VME::Close();

    // print close statement
    DEBUG_TEXT(DFDB_RCDTTC,15,"RCD::TTCvi library closed");
}

//------------------------------------------------------------------------------

u_int TTCVI::reset(bool rst) {

    // reset TTCvi
    m_dats = (rst) ? 0xffff : 0x0000;
    if((m_status = m_vmm->WriteSafe(REG_RESET,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"resetting TTCVI");
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::dump() {

    u_int	data;
    u_short	del, dur;
    int		ival;
    LongCommand	lcmd;
    bool	flag;
    int 	i;

    // dump configuration/identifiation EEPROM ---------------------------------
    std::cout << "configuration/identification EEPROM:" << std::endl;

    // read manufacturer ID
    if((m_status = manufacturerGet(&data)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading manufacturer ID");
	return(m_status);
    }
    printf("                       manufacturer ID =   %06x\n",data);

    // read board ID
    if((m_status = boardIdentifierGet(&data)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading board ID");
	return(m_status);
    }
    printf("                       board ID        = %08x\n",data);

    // read board revision
    if((m_status = boardRevisionGet(&data)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading board revision");
	return(m_status);
    }
    printf("                       board revision  = %08x\n",data);

    // get Mk type
    printf("                       Mk type         = %8d\n",m_type);

    // dump Control and Status register #1 -------------------------------------
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading from CSR1");
	return(m_status);
    }

    // dump CSR1 parameters
    printf("CSR1     =   %04x\n",m_dats);
    std::cout << "                       counter select  = ";
    (m_type == MK_TYP1) ?  std::cout << "     L1A" :
	((m_dats&MSK_CSR1_CNT) ? std::cout << "   ORBIT" : std::cout << "     L1A");
    std::cout << std::endl;
    printf("                       rndm trig rate  =  %7s\n",
	RNDM_NAME[(m_dats&MSK_CSR1_RND)>>SHF_CSR1_RND].c_str());
    printf("                       BC delay        = %5d ns\n",
	((((~m_dats)&MSK_CSR1_BC))>>SHF_CSR1_BC)*CNV_CSR1_BC);
    std::cout << "                       ASYNC pending   = ";
    (m_dats&MSK_CSR1_VME) ? std::cout << "     yes" : std::cout << "      no";
    std::cout << std::endl;
    std::cout << "                       L1A FIFO        = ";
    if(m_dats&MSK_CSR1_EMP) std::cout << "   empty";
    if(m_dats&MSK_CSR1_FUL) std::cout << "    full";
    std::cout << std::endl;
    std::cout << "                       ORBIT select    = ";
    (m_dats&MSK_CSR1_ORB) ?  std::cout << "internal" : std::cout << "external";
    std::cout << std::endl;
    printf("                       L1A select      = %8s\n",
	L1A_NAME[m_dats&MSK_CSR1_L1A].c_str());

    // dump trigger word address -----------------------------------------------
    if(m_type == MK_TYP2) {
	std::cout << "trigger word address:" << std::endl;
	if((m_status = triggerWordGet(&lcmd,&flag)) != VME_SUCCESS) {
	    ERR_TEXT(DFDB_RCDTTC,"reading trigger word");
	    return(m_status);
	}
	printf("                       address         =     %04x\n",lcmd.address);
	printf("                       external        =        %d\n",lcmd.external);
	printf("                       sub-address     =       %02x\n",lcmd.sub_address);
	printf("                       enable (size)   =        %d\n",(int)flag);
    }

    // dump Control and Status register #2 -------------------------------------
    if((m_status = m_vmm->ReadSafe(REG_CSR2,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading from CSR2");
	return(m_status);
    }

    // dump CSR2 parameters
    printf("CSR2     =   %04x\n",m_dats);
    for(i=0; i<(BGO_NUM); i++) {
	printf("                       BGO channel %d   = ",i);
	if(m_dats&(BIT_CSR2_FIFO << (i*2))) std::cout << "   empty";
	if(m_dats&(BIT_CSR2_FIFO << (i*2+1))) std::cout << "    full";
	if((m_dats&(BIT_CSR2_RTRS << i)) == 0) std::cout << " retransmit";
	std::cout << std::endl;
    }

    // dump BGO channels -------------------------------------------------------
    for(int i=0; i<BGO_NUM; i++) {

	// read BGO mode
	if((m_status = bgoModeGet(i,&m_dats)) != VME_SUCCESS) {
	    ERR_TEXT(DFDB_RCDTTC,"reading BGO mode");
	    return(m_status);
	}

	(m_type == MK_TYP1) ?
	    printf("BGO%d     =      %01x     ",i,(~m_dats&MSK_BMOD)) :
	    printf("BGO%d     =     %02x     ",i,(~m_dats&MSK_BMOD)|(m_dats&BGO_CALIB));
	(m_dats&BGO_ENABLE) ? std::cout << " EXTERNAL" : std::cout << " VME";
	(m_dats&BGO_SYNC) ? std::cout << " SYNC" : std::cout << " ASYNC";
	(m_dats&BGO_SINGLE) ? std::cout << " SINGLE" : std::cout << " REPETITIVE";
	(m_dats&BGO_FIFO) ? std::cout << " FIFO" : std::cout << " NO_FIFO";
	if(m_type == MK_TYP2 && i == 2)
	    (m_dats&BGO_CALIB) ? std::cout << " CALIBRATION" : std::cout << " BASIC";
	std::cout << std::endl;

	// read inhibit parameters
	if((m_status = bgoInhibitGet(i,&del,&dur)) != VME_SUCCESS) {
	    ERR_TEXT(DFDB_RCDTTC,"reading inhibit parameters");
	    return(m_status);
	}
	printf("                       inhibit del = %03x (%6d ns),",del,del*CNV_INH);
	printf(" dur = %02x (%4d ns)\n",dur,dur*CNV_INH);
    }

    // dump Event/Orbit counter ------------------------------------------------
    counterValueGet(&ival);
    if((m_status = counterValueGet(&ival)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading Event/Orbit counter");
	return(m_status);
    }
    printf("counter  = %06x      (= %08d dec)\n",ival,ival);
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::manufacturerGet(u_int* manu) {

    int		i;

    // read EEPROM for manufacturer ID
    *manu = 0;
    for(i=0; i<3; i++) {
	m_addr = REG_MANUF_ID+4*i;
	if((m_status = m_vmm->ReadSafe(m_addr,&m_dats)) != VME_SUCCESS) {
	    ERR_TEXT(DFDB_RCDTTC,"reading from conf/id EEPROM");
	    return(m_status);
	}
	*manu = ((*manu)<<8) | (m_dats&0xff);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::boardIdentifierGet(u_int* board) {

    int		i;

    // read EEPROM for board ID
    *board = 0;
    for(i=0; i<4; i++) {
	m_addr = REG_BOARD_ID+4*i;
	if((m_status = m_vmm->ReadSafe(m_addr,&m_dats)) != VME_SUCCESS) {
	    ERR_TEXT(DFDB_RCDTTC,"reading from conf/id EEPROM");
	    return(m_status);
	}
	*board = ((*board)<<8) | (m_dats&0xff);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::boardRevisionGet(u_int* rev) {

    int		i;

    // read EEPROM for board revision
    *rev = 0;
    for(i=0; i<4; i++) {
	m_addr = REG_BOARD_RV+4*i;
	if((m_status = m_vmm->ReadSafe(m_addr,&m_dats)) != VME_SUCCESS) {
	    ERR_TEXT(DFDB_RCDTTC,"reading from conf/id EEPROM");
	    return(m_status);
	}
	*rev = ((*rev)<<8) | (m_dats&0xff);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::mkTypeGet(int* typ) {

    *typ = m_type;
    return(VME_SUCCESS);
}

//------------------------------------------------------------------------------

u_int TTCVI::bcDelayGet(int* del) {


    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR1");
	return(m_status);
    }
    *del = (((~m_dats) & MSK_CSR1_BC) >> SHF_CSR1_BC) * CNV_CSR1_BC;

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::orbitInputSet(u_short flg) {

    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR1");
	return(m_status);
    }

    // set ORBIT input selection
    m_dats = (m_dats & ~MSK_CSR1_ORB) | (flg & MSK_CSR1_ORB);

    // write CSR1
    if((m_status = m_vmm->WriteSafe(REG_CSR1,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing CSR1");
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::orbitInputGet(u_short* flg) {

    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR1");
	return(m_status);
    }
    *flg = (m_dats & MSK_CSR1_ORB);

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::triggerWordEnable(LongCommand& cmd) {

    // check Mk type
    if(m_type == MK_TYP1) {
	ERR_TEXT(DFDB_RCDTTC,"not permitted for Mk I type");
	return(EPERM);
    }

    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << cmd.address
	       << ", ext = " << cmd.external
	       << ", sub = " << (u_short)cmd.sub_address
	       << ", unused dat = " << (u_short)cmd.data << std::dec);

    // write address
    m_dats = cmd.address&MSK_TWRD_ADDR;
    if((m_status = m_vmm->WriteSafe(REG_TWRD_HI,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing trigger word address HI");
	return(m_status);
    }

    // write size flag (=enable), external flag and sub-address
    m_dats = (cmd.sub_address&MSK_TWRD_SADD) | BIT_TWRD_SIZE;
    if(cmd.external) m_dats |= BIT_TWRD_EXT;
    if((m_status = m_vmm->WriteSafe(REG_TWRD_LO,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing trigger word address LO");
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::triggerWordDisable() {

    // check Mk type
    if(m_type == MK_TYP1) {
	ERR_TEXT(DFDB_RCDTTC,"not permitted for Mk I type");
	return(EPERM);
    }

    // reset enable, external flag and sub-address
    m_dats = 0;
    if((m_status = m_vmm->WriteSafe(REG_TWRD_LO,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing trigger word address LO");
	return(m_status);
    }

    // reset address
    m_dats = 0;
    if((m_status = m_vmm->WriteSafe(REG_TWRD_HI,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing trigger word address HI");
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::triggerWordGet(LongCommand* cmd, bool* ena) {

    // check Mk type
    if(m_type == MK_TYP1) {
	ERR_TEXT(DFDB_RCDTTC,"not permitted for Mk I type");
	return(EPERM);
    }

    // read external flag and sub-address
    if((m_status = m_vmm->ReadSafe(REG_TWRD_LO,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading trigger word address LO");
	return(m_status);
    }
    cmd->sub_address = m_dats&MSK_TWRD_SADD;
    cmd->external = m_dats&BIT_TWRD_EXT;
    *ena = m_dats&BIT_TWRD_SIZE;

    // read address
    if((m_status = m_vmm->ReadSafe(REG_TWRD_HI,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading trigger word address HI");
	return(m_status);
    }
    cmd->address = m_dats&MSK_TWRD_ADDR;

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::counterValueGet(int* data) {

    // read Event/Orbit counter, upper half
    if((m_status = m_vmm->ReadSafe(REG_CNT_HI,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading  Event/Orbit counter HI");
	return(m_status);
    }
    *data = (m_dats & MSK_CNT_HI) << SHF_CNT_HI;

    // read Event/Orbit counter, lower half
    if((m_status = m_vmm->ReadSafe(REG_CNT_LO,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading Event/Orbit counter LO");
	return(m_status);
    }
    *data |= m_dats & MSK_CNT_LO;

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::counterSelectionSet(u_short sel) {

    // check Mk type
    if(m_type == MK_TYP1) {
	ERR_TEXT(DFDB_RCDTTC,"not permitted for Mk I type");
	return(EPERM);
    }

    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR1");
	return(m_status);
    }

    // set Event/Orbit counter selection
    m_dats = (m_dats & ~MSK_CSR1_CNT) | (sel & MSK_CSR1_CNT);

    // write CSR1
    if((m_status = m_vmm->WriteSafe(REG_CSR1,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing CSR1");
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::counterSelectionGet(u_short* sel) {

    // if Mk I then always return L1A
    if(m_type == MK_TYP1) {
	*sel = CNT_L1A;
	return(m_status);
    }

    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR1");
	return(m_status);
    }
    *sel = m_dats & MSK_CSR1_CNT;

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::counterReset() {

    // check Mk type
    if(m_type == MK_TYP1) {
	ERR_TEXT(DFDB_RCDTTC,"not permitted for Mk I type");
	return(EPERM);
    }

    // reset Event/Orbit counter
    if((m_status = m_vmm->WriteSafe(REG_CNT_RST,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"resetting Event/Orbit counter");
    }
    return(m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

u_int TTCVI::l1aInputSet(u_short inp) {

    // check input selection
    if(inp > MAX_CSR1_L1A) {
	ERR_TEXT(DFDB_RCDTTC,"illegal l1a input selection \"" << std::dec << inp << "\"");
	return(EINVAL);
    }

    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR1");
	return(m_status);
    }

    // set L1A input selection
    m_dats = (m_dats & ~MSK_CSR1_L1A) | (inp & MSK_CSR1_L1A);

    // write CSR1
    if((m_status = m_vmm->WriteSafe(REG_CSR1,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing CSR1");
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::l1aInputGet(u_short* inp) {

    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR1");
	return(m_status);
    }
    *inp = m_dats & MSK_CSR1_L1A;

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::l1aRandomSet(u_short rnd) {

    // check random selection
    if(rnd > MAX_CSR1_RND) {
	ERR_TEXT(DFDB_RCDTTC,"illegal random generator frequency selection \"" << std::dec << rnd << "\"");
	return(EINVAL);
    }

    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR1");
	return(m_status);
    }

    // set L1A random generator frequency selection
    m_dats = (m_dats & ~MSK_CSR1_RND) | ((rnd << SHF_CSR1_RND) & MSK_CSR1_RND);

    // write CSR1
    if((m_status = m_vmm->WriteSafe(REG_CSR1,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing CSR1");
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::l1aRandomGet(u_short* inp) {

    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR1");
	return(m_status);
    }
    *inp = (m_dats & MSK_CSR1_RND) >> SHF_CSR1_RND;

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::l1aGenerate() {

    // generate L1A
    if((m_status = m_vmm->WriteSafe(REG_L1A,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing L1A");
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::l1aFifoEmpty(bool* emp) {

    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	DEBUG_TEXT(DFDB_RCDTTC,15,"reading CSR1");
	return(m_status);
    }
    *emp = (m_dats & MSK_CSR1_EMP);

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::l1aFifoFull(bool* ful) {

    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	DEBUG_TEXT(DFDB_RCDTTC,15,"reading CSR1");
	return(m_status);
    }
    *ful = (m_dats & MSK_CSR1_FUL);

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::l1aFifoReset() {

    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	DEBUG_TEXT(DFDB_RCDTTC,15,"reading CSR1");
	return(m_status);
    }

    // reset L1A FIFO
    m_dats = (m_dats & ~MSK_CSR1_RST) | MSK_CSR1_RST;
    if((m_status = m_vmm->WriteSafe(REG_CSR1,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing CSR1");
    }
    return(m_status);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

u_int TTCVI::bgoModeSet(int chn, u_short mode) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM) {
	ERR_TEXT(DFDB_RCDTTC,"illegal BGO channel number " << std::dec << chn);
	return(EINVAL);
    }

    // check mode
    if(mode > MAX_BMOD) {
	ERR_TEXT(DFDB_RCDTTC,"illegal bgo mode \"" << std::hex << mode << std::dec << "\"");
	return(EINVAL);
    }

    // check Mk type
    if((mode&BGO_CALIB) && (m_type == MK_TYP1)) {
	ERR_TEXT(DFDB_RCDTTC,"not permitted for Mk I type");
	return(EPERM);
    }

    // check calibration mode only for channel 2
    if((mode&BGO_CALIB) && (chn != 2)) {
	ERR_TEXT(DFDB_RCDTTC,"calibration mode not permitted for channel number " << std::dec << chn);
	return(EPERM);
    }

    // write BGO mode selection
    m_addr = REG_BMOD + chn*STP_BGO;
    m_dats = (mode&BGO_CALIB) ?
	     (~(mode&MSK_BMOD)) |   BGO_CALIB :
	     (~(mode&MSK_BMOD)) & (~BGO_CALIB);
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << m_addr
	       << ", data = " << m_dats << std::dec);
    if((m_status = m_vmm->WriteSafe(m_addr,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing BGO mode for channel " << std::dec << chn);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoModeGet(int chn, u_short* mode) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM) {
	ERR_TEXT(DFDB_RCDTTC,"illegal BGO channel number " << std::dec << chn);
	return(EINVAL);
    }

    // read BGO mode selection
    m_addr = REG_BMOD + chn*STP_BGO;
    if((m_status = m_vmm->ReadSafe(m_addr,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading BGO mode for channel " << std::dec << chn);
	return(m_status);
    }
    *mode = ((~m_dats) & MSK_BMOD) | (m_dats & BGO_CALIB);

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoCommandPut(int chn, ShortCommand& cmd) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM) {
	ERR_TEXT(DFDB_RCDTTC,"illegal bgo channel number " << std::dec << chn);
	return(EINVAL);
    }

    // write data to BGO FIFO
    m_addr = REG_BDAT + chn*STP_BDAT;
    u_int data = (cmd&MSK_BDAT_SHORT) << SHF_BDAT_SHORT;
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << m_addr
	       << ", data = " << std::setfill('0') << std::setw(8) << data << std::dec);
    if((m_status = m_vmm->WriteSafe(m_addr,data)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing data " << std::hex << std::setfill('0') << std::setw(8) << data << std::dec
		 << " to BGO FIFO " << chn);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoCommandPut(int chn, LongCommand& cmd) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM) {
	ERR_TEXT(DFDB_RCDTTC,"illegal bgo channel number " << std::dec << chn);
	return(EINVAL);
    }

    // write data to BGO FIFO
    m_addr = REG_BDAT + chn*STP_BDAT;
    u_int data = BIT_BDAT_LONG;
    data |= ((cmd.address&MSK_BDAT_ADDR) << SHF_BDAT_ADDR) |
	    ((cmd.sub_address&MSK_BDAT_DATA) << SHF_BDAT_SUBA) |
	    (cmd.data&MSK_BDAT_DATA);
    if(cmd.external) data |= BIT_BDAT_EXT;
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << m_addr
	       << ", data = " << std::setfill('0') << std::setw(8) << data << std::dec);
    if((m_status = m_vmm->WriteSafe(m_addr,data)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing data " << std::hex << std::setfill('0') << std::setw(8) << data << std::dec
		 << " to BGO FIFO " << chn);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoCommandPut(int chn, u_int data) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM) {
	ERR_TEXT(DFDB_RCDTTC,"illegal bgo channel number " << std::dec << chn);
	return(EINVAL);
    }

    // write data to BGO FIFO
    m_addr = REG_BDAT + chn*STP_BDAT;
    data = data & 0xffffffff;
    if((m_status = m_vmm->WriteSafe(m_addr,data)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing data " << std::hex << std::setfill('0') << std::setw(8) << data << std::dec
		 << " to BGO FIFO " << chn);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoGenerate(int chn) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM-1) {
	ERR_TEXT(DFDB_RCDTTC,"illegal bgo channel number " << std::dec << chn);
	return(EINVAL);
    }

    // generate BGO
    m_addr = REG_BGO + chn*STP_BGO;
    DEBUG_TEXT(DFDB_RCDTTC,20,"writing to address " << std::hex << m_addr << std::dec);
    if((m_status = m_vmm->WriteSafe(m_addr,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing BGO Generate for channel " << std::dec << chn);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoInhibitOn(int chn, u_short del, u_short dur) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM-1) {
	ERR_TEXT(DFDB_RCDTTC,"illegal inhibt number \"" << std::dec << chn << "\"");
	return(EINVAL);
    }

    // check inhibit delay
    if(del > MSK_IDEL) {
	ERR_TEXT(DFDB_RCDTTC,"illegal inhibt delay \"" << std::dec << del << "\"");
	return(EINVAL);
    }

    // check inhibit duration
    if(dur > MSK_IDUR) {
	ERR_TEXT(DFDB_RCDTTC,"illegal inhibt duration \"" << std::dec << dur << "\"");
	return(EINVAL);
    }

    // write inhibit delay first
    m_addr = REG_IDEL + chn*STP_BGO;
    m_dats = del & 0xfff;
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << m_addr
	       << ", data = " << m_dats << std::dec);
    if((m_status = m_vmm->WriteSafe(m_addr,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing inhibit delay \"" << std::dec << del
		 << "\" to channel " << chn);
	return(m_status);
    }

    // write inhibit duration second
    m_addr = REG_IDUR + chn*STP_BGO;
    m_dats = dur & 0xff;
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << m_addr
	       << ", data = " << m_dats << std::dec);
    if((m_status = m_vmm->WriteSafe(m_addr,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing inhibit duration \"" << del << dur
		 << "\" to channel " << chn);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoInhibitOff(int chn) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM-1) {
	ERR_TEXT(DFDB_RCDTTC,"illegal inhibt number \"" << std::dec << chn << "\"");
	return(EINVAL);
    }

    // write inhibit duration first
    m_addr = REG_IDUR + chn*STP_BGO;
    m_dats = 0;
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << m_addr
	       << ", data = " << m_dats << std::dec);
    if((m_status = m_vmm->WriteSafe(m_addr,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing inhibit duration \"0\" to channel " << std::dec << chn);
	return(m_status);
    }

    // write inhibit delay second
    m_addr = REG_IDEL + chn*STP_BGO;
    m_dats = 0;
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << m_addr
	       << ", data = " << m_dats << std::dec);
    if((m_status = m_vmm->WriteSafe(m_addr,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing inhibit delay \"0\" to channel " << std::dec << chn);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoInhibitGet(int chn, u_short* del, u_short* dur) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM-1) {
	ERR_TEXT(DFDB_RCDTTC,"illegal inhibit number \"" << std::dec << chn << "\"");
	return(EINVAL);
    }

    // read inhibit delay
    m_addr = REG_IDEL + chn*STP_BGO;
    if((m_status = m_vmm->ReadSafe(m_addr,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading inhibit delay \"" << std::dec << del
		 << "\" from channel " << chn);
	return(m_status);
    }
    *del = m_dats & MSK_IDEL;

    // read inhibit duration
    m_addr = REG_IDUR + chn*STP_BGO;
    if((m_status = m_vmm->ReadSafe(m_addr,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading inhibit duration \"" << std::dec << dur
		 << "\" from channel " << chn);
	return(m_status);
    }
    *dur = m_dats & MSK_IDUR;

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoFifoEmpty(int chn, bool* emp) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM) {
	ERR_TEXT(DFDB_RCDTTC,"illegal BGO channel number " << std::dec << chn);
	return(EINVAL);
    }

    // read CSR2
    if((m_status = m_vmm->ReadSafe(REG_CSR2,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR2");
	return(m_status);
    }
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << REG_CSR2
	       << ", data = " << m_dats << std::dec);
    *emp = (m_dats & (BIT_CSR2_FIFO << (2*chn)));

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoFifoFull(int chn, bool* ful) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM) {
	ERR_TEXT(DFDB_RCDTTC,"illegal BGO channel number " << std::dec << chn);
	return(EINVAL);
    }

    // read CSR2
    if((m_status = m_vmm->ReadSafe(REG_CSR2,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR2");
	return(m_status);
    }
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << REG_CSR2
	       << ", data = " << m_dats << std::dec);
    *ful = (m_dats & (BIT_CSR2_FIFO <<(2*chn + 1)));

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoFifoRetransSet(int chn, bool flg) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM) {
	ERR_TEXT(DFDB_RCDTTC,"illegal BGO channel number " << std::dec << chn);
	return(EINVAL);
    }

    // read CSR2
    if((m_status = m_vmm->ReadSafe(REG_CSR2,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR2");
	return(m_status);
    }
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << REG_CSR2
	       << ", data = " << m_dats << std::dec);

    // set retransmit flag
    m_dats = flg ? (m_dats & MSK_CSR2_RTRS) & ~(BIT_CSR2_RTRS << chn) :
	           (m_dats & MSK_CSR2_RTRS) |  (BIT_CSR2_RTRS << chn);
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << REG_CSR2
	       << ", data = " << m_dats << std::dec);
    if((m_status = m_vmm->WriteSafe(REG_CSR2,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing CSR2");
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoFifoRetransGet(int chn, bool* flg) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM) {
	ERR_TEXT(DFDB_RCDTTC,"illegal BGO channel number " << std::dec << chn);
	return(EINVAL);
    }

    // read CSR2
    if((m_status = m_vmm->ReadSafe(REG_CSR2,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR2");
	return(m_status);
    }
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << REG_CSR2
	       << ", data = " << m_dats << std::dec);
    *flg = ((m_dats & (BIT_CSR2_RTRS << chn)) == 0);

    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::bgoFifoReset(int chn) {

    // check BGO channel number
    if(chn < 0 || chn > BGO_NUM) {
	ERR_TEXT(DFDB_RCDTTC,"illegal BGO channel number " << std::dec << chn);
	return(EINVAL);
    }

    // read CSR2
    if((m_status = m_vmm->ReadSafe(REG_CSR2,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR2");
	return(m_status);
    }

    // reset BGO<chn> FIFO
    m_dats = (m_dats & MSK_CSR2_RTRS) | (BIT_CSR2_RSET << chn);
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << REG_CSR2
	       << ", data = " << m_dats << std::dec);
    if((m_status = m_vmm->WriteSafe(REG_CSR2,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing CSR2");
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::asyncCommand(ShortCommand& cmd) {

    m_dats = cmd&MSK_SHORT_DATA;
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << REG_SHORT
	       << ", data = " << m_dats << std::dec);
    if((m_status = m_vmm->WriteSafe(REG_SHORT,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing ASYNC SHORT");
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::asyncCommand(LongCommand& cmd) {

    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << cmd.address
	       << ", ext = " << cmd.external
	       << ", sub = " << (u_short)cmd.sub_address
	       << ", dat = " << (u_short)cmd.data << std::dec);

    // write address
    m_dats = BIT_LONG_MARK;
    m_dats |= (cmd.address&MSK_BDAT_ADDR) << STP_LONG_ADDR;
    if(cmd.external) m_dats |= BIT_LONG_EXT;
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << REG_LONG_HI
	       << ", data = " << m_dats << std::dec);
    if((m_status = m_vmm->WriteSafe(REG_LONG_HI,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing long command HI");
	return(m_status);
    }

    // write external flag and sub-address
    m_dats = ((cmd.sub_address&MSK_BDAT_DATA) << SHF_BDAT_SUBA) | (cmd.data&MSK_BDAT_DATA);
    DEBUG_TEXT(DFDB_RCDTTC,20,"addr = " << std::hex << REG_LONG_LO
	       << ", data = " << m_dats << std::dec);
    if((m_status = m_vmm->WriteSafe(REG_LONG_LO,m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"writing long command LO");
    }
    return(m_status);
}

//------------------------------------------------------------------------------

u_int TTCVI::asyncPendingGet(bool* flg) {

    // read CSR1
    if((m_status = m_vmm->ReadSafe(REG_CSR1,&m_dats)) != VME_SUCCESS) {
	ERR_TEXT(DFDB_RCDTTC,"reading CSR1");
	return(m_status);
    }
    *flg = (m_dats & MSK_CSR1_VME);

    return(m_status);
}
