//******************************************************************************
  // file: menuRCDTtcvi.cc
  // desc: menu for test of library for TTCVI
  // auth: 02/05/00 R. Spiwoks
  // modf: 05/12/02 R. Spiwoks, DataFlow repository
  // modf: 06/12/02 R. Spiwoks, new user interface
  // modf: 31/01/03 R. Spiwoks, distinction MkI/MkII, clean-up
  // modf: 23/01/04 M. Gruwe: Fixup (specify std:: when necessary)
  // modf: 18/06/04 L. Levinson: fix for calibration mode
  //******************************************************************************

    // $Id$

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <cerrno>

#include "DFDebug/DFDebug.h"
#include "RCDMenu/RCDMenu.h"
#include "RCDTtc/RCDTtcvi.h"

#include "rcc_time_stamp/tstamp.h"
#include "rcc_error/rcc_error.h"
#include "cmdl/cmdargs.h"


  using namespace RCD;

// global variable
TTCVI*		ttcvi;
u_int base = 0;

// global helpers
u_int		rtnv;
u_int		dati;
u_short		dats;
int		valu;


//------------------------------------------------------------------------------

class TTCVIOpen : public MenuItem {
public:
  TTCVIOpen() { setName("open ttcvi"); }
  int action() {
    std::string		in;

    DF::GlobalDebugSettings::setup(20,DFDB_RCDTTC);

    ttcvi = new TTCVI(base);

    return((*ttcvi)());
  }
};

//------------------------------------------------------------------------------

class TTCVIClose : public MenuItem {
public:
  TTCVIClose() { setName("close ttcvi"); }
  int action() {

    delete ttcvi;

    return(0);
  }
};

//------------------------------------------------------------------------------

class TTCVIReset : public MenuItem {
public:
  TTCVIReset() { setName("reset"); }
  int action() {

    bool rst = (bool) enterInt("reset FIFOs",0,1);
    return(ttcvi->reset(rst));
  }
};

//------------------------------------------------------------------------------

class TTCVIDump : public MenuItem {
public:
  TTCVIDump() { setName("dump"); }
  int action() {

    return(ttcvi->dump());
  }
};

//------------------------------------------------------------------------------

class TTCVIGENBcDelayGet : public MenuItem {
public:
  TTCVIGENBcDelayGet() { setName("get BC delay"); }
  int action() {

    if((rtnv = ttcvi->bcDelayGet(&valu)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVIGENBcDelayGet: BC delay = " << valu << std::endl;

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIGENOrbitInputSet : public MenuItem {
public:
  TTCVIGENOrbitInputSet() { setName("set ORBIT input selection"); }
  int action() {
    bool flag = (bool) enterInt("external",0,1);
    // dats = (flag) ? TTCVI::ORB_EXT : TTCVI::ORB_INT;
    if(flag)
      dats = TTCVI::ORB_EXT;
    else
      dats = TTCVI::ORB_INT;

    return(ttcvi->orbitInputSet(dats));
  }
};

//------------------------------------------------------------------------------

class TTCVIGENOrbitInputGet : public MenuItem {
public:
  TTCVIGENOrbitInputGet() { setName("get ORBIT input selection"); }
  int action() {

    if((rtnv = ttcvi->orbitInputGet(&dats)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVIGENOrbitInputGet: ORBIT input selection = ";
    (dats==TTCVI::ORB_EXT) ? std::cout << "external" : std::cout << "internal";
    std::cout << std::endl;

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVITrigWordEnable : public MenuItem {
public:
  TTCVITrigWordEnable() { setName("enable trigger word"); }
  int action() {

    TTCVI::LongCommand	cmd;
    cmd.address = enterHex("address      ",0x0000,0x3fff);
    cmd.external = bool(enterInt("external     ",0,1));
    cmd.sub_address = (u_char)enterHex("sub-address  ",0x00,0xff);
    return(ttcvi->triggerWordEnable(cmd));
  }
};

//------------------------------------------------------------------------------

class TTCVITrigWordDisable : public MenuItem {
public:
  TTCVITrigWordDisable() { setName("disable trigger word"); }
  int action() {

    return(ttcvi->triggerWordDisable());
  }
};

//------------------------------------------------------------------------------

class TTCVITrigWordGet : public MenuItem {
public:
  TTCVITrigWordGet() { setName("get trigger word parameters"); }
  int action() {

    TTCVI::LongCommand	cmd;
    cmd.address = 0;
    cmd.external = 0;
    cmd.sub_address = 0;
    bool flg;
    rtnv = ttcvi->triggerWordGet(&cmd,&flg);
    if(rtnv == 0) {
      printf("address     = %04x\n",cmd.address);
      printf("external    = %d\n",cmd.external);
      printf("sub_address = %02x\n",cmd.sub_address);
      printf("enable      = %02x\n",flg);
    }

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVICounterGet : public MenuItem {
public:
  TTCVICounterGet() { setName("get Event/Orbit counter value"); }
  int action() {

    if((rtnv = ttcvi->counterValueGet(&valu)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVICounterGet: Event/Orbit counter = " << valu << std::endl;

    return(0);
  }
};

//------------------------------------------------------------------------------

class TTCVICounterSelectionSet : public MenuItem {
public:
  TTCVICounterSelectionSet() { setName("set Event/Orbit counter selection"); }
  int action() {

    dats = enterHex("counter selection (0-l1a,1-orb)",0,0x1);
    // dats = (dats) ? TTCVI::CNT_ORB : TTCVI::CNT_L1A;
    if(dats)
      dats = TTCVI::CNT_ORB;
    else
      dats = TTCVI::CNT_L1A;
    return(ttcvi->counterSelectionSet(dats));
  }
};

//------------------------------------------------------------------------------

class TTCVICounterSelectionGet : public MenuItem {
public:
  TTCVICounterSelectionGet() { setName("get Event/Orbit counter selection"); }
  int action() {

    if((rtnv = ttcvi->counterSelectionGet(&dats)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVICounterSelectionGet: Event/Orbit counter selection = ";
    (dats==TTCVI::CNT_ORB) ? std::cout << "ORBIT" : std::cout << "L1A";
    std::cout << std::endl;

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVICounterReset : public MenuItem {
public:
  TTCVICounterReset() { setName("reset Event/Orbit counter"); }
  int action() {

    return(ttcvi->counterReset());
  }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class TTCVIL1aInputSet : public MenuItem {
public:
  TTCVIL1aInputSet() { setName("set L1A input selection"); }
  int action() {

    dats = enterHex("input selection ([0..3]-ext,4-vme,5-rnd,6-calib,7-disable)",0,0x7);  // LL 18-Jun-04

    return(ttcvi->l1aInputSet(dats));
  }
};

//------------------------------------------------------------------------------

class TTCVIL1aInputGet : public MenuItem {
public:
  TTCVIL1aInputGet() { setName("get L1A input selection"); }
  int action() {

    if((rtnv = ttcvi->l1aInputGet(&dats)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVIL1aInputGet: L1A input selection = " << dats << std::endl;

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIL1aRandomSet : public MenuItem {
public:
  TTCVIL1aRandomSet() { setName("set L1A random generator frequency"); }
  int action() {

    dats = enterHex("random generator frequency (0-1Hz,...7-100kHz)",0,0x7);

    return(ttcvi->l1aRandomSet(dats));
  }
};

//------------------------------------------------------------------------------

class TTCVIL1aRandomGet : public MenuItem {
public:
  TTCVIL1aRandomGet() { setName("get L1A random generator frequency"); }
  int action() {

    if((rtnv = ttcvi->l1aRandomGet(&dats)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVIL1aRandomGet: L1A random generator frequency = " << TTCVI::RNDM_NAME[(int)dats] << std::endl;

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIL1aGenerate : public MenuItem {
public:
  TTCVIL1aGenerate() { setName("generate L1A"); }
  int action() {
    int	i;

    int loop = enterInt("how many times",0,1000000);

    for(i=0; i<loop; i++) {
      if((rtnv = ttcvi->l1aGenerate()) != 0) {
	return(rtnv);
      }
    }

    return(0);
  }
};

//------------------------------------------------------------------------------

class TTCVIL1aFifoEmpty : public MenuItem {
public:
  TTCVIL1aFifoEmpty() { setName("check if L1A FIFO empty"); }
  int action() {
    bool	empty;

    if((rtnv = ttcvi->l1aFifoEmpty(&empty)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVIL1aFifoEmpty: L1A FIFO empty = " << empty << std::endl;

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIL1aFifoFull : public MenuItem {
public:
  TTCVIL1aFifoFull() { setName("check if L1A FIFO full"); }
  int action() {
    bool	full;

    if((rtnv = ttcvi->l1aFifoFull(&full)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVIL1aFifoFull: L1A FIFO full = " << full << std::endl;

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIL1aFifoReset : public MenuItem {
public:
  TTCVIL1aFifoReset() { setName("reset L1A FIFO"); }
  int action() {

    return(ttcvi->l1aFifoReset());
  }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class TTCVIBgoModeSet : public MenuItem {
public:
  TTCVIBgoModeSet() { setName("set BGO mode"); }
  int action() {

    int chn = enterInt("BGO channel                               ",0,TTCVI::BGO_NUM-1);
    int data = enterHex("BGO mode (01-enable,02-sync,04-single,08-fifo,10-cal)",0x0,0x1f);

    return(ttcvi->bgoModeSet(chn,data));
  }
};

//------------------------------------------------------------------------------

class TTCVIBgoModeGet : public MenuItem {
public:
  TTCVIBgoModeGet() { setName("get BGO mode"); }
  int action() {

    int chn = enterInt("BGO channel",0,TTCVI::BGO_NUM-1);
    if((rtnv = ttcvi->bgoModeGet(chn,&dats)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVIBGOModeGet: BGO mode =";
    (dats&TTCVI::BGO_FIFO) ? std::cout << " FIFO" : std::cout << " NO_FIFO";
    (dats&TTCVI::BGO_SINGLE) ? std::cout << " SINGLE" : std::cout << " REPETITIVE";
    (dats&TTCVI::BGO_SYNC) ? std::cout << " SYNC": std::cout << " ASYNC";
    (dats&TTCVI::BGO_ENABLE) ? std::cout << " ENABLE" : std::cout << " VME";
    (dats&TTCVI::BGO_CALIB) ? std::cout << " CALIBRATION" : std::cout << " BASIC";
    std::cout << std::endl;

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIBgoCommandPut : public MenuItem {
public:
  TTCVIBgoCommandPut() { setName("put BGO command"); }
  int action() {

    int chn = enterInt("BGO channel",0,TTCVI::BGO_NUM-1);
    int sht = enterInt("short      ",0,1);

    if(sht) {
      TTCVI::ShortCommand cmd =
	(TTCVI::ShortCommand) enterHex("command    ",0x00,0xff);
      rtnv = ttcvi->bgoCommandPut(chn,cmd);
    }
    else {
      TTCVI::LongCommand	cmd;
      cmd.address = enterHex("address    ",0x0000,0x3fff);
      cmd.external = bool(enterInt("external   ",0,1));
      cmd.sub_address = (u_char)enterHex("sub-address",0x00,0xff);
      cmd.data = (u_char)enterHex("data       ",0x00,0xff);
      rtnv = ttcvi->bgoCommandPut(chn,cmd);
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIBgoGenerate : public MenuItem {
public:
  TTCVIBgoGenerate() { setName("generate BGO"); }
  int action() {

    int chn = enterInt("BGO channel",0,TTCVI::BGO_NUM-1);
    int loop = enterInt("how many times",0,1000000);

    for(int i=0; i<loop; i++) {
      if((rtnv = ttcvi->bgoGenerate(chn)) != 0) {
	return(rtnv);
      }
    }

    return(0);
  }
};

//------------------------------------------------------------------------------

class TTCVIBgoInhibitOn : public MenuItem {
public:
  TTCVIBgoInhibitOn() { setName("switch inhibit channel on"); }
  int action() {

    int chn = enterInt("BGO channel",0,TTCVI::BGO_NUM-1);
    int del = enterHex("delay      ",0x000,0xfff);
    int dur = enterHex("duration   ",0x000,0xff);

    return(ttcvi->bgoInhibitOn(chn,del,dur));
  }
};

//------------------------------------------------------------------------------

class TTCVIBgoInhibitOff : public MenuItem {
public:
  TTCVIBgoInhibitOff() { setName("switch inhibit channel off"); }
  int action() {

    int chn = enterInt("BGO channel",0,TTCVI::BGO_NUM-1);

    return(ttcvi->bgoInhibitOff(chn));
  }
};

//------------------------------------------------------------------------------

class TTCVIBgoInhibitGet : public MenuItem {
public:
  TTCVIBgoInhibitGet() { setName("get inhibit channel parameters"); }
  int action() {
    u_short	del, dur;

    int	chn = enterInt("BGO channel",0,TTCVI::BGO_NUM-1);

    if((rtnv = ttcvi->bgoInhibitGet(chn,&del,&dur)) != 0) {
      return(rtnv);
    }
    printf("TTCVIBgoInhibitGet: BGO chn = %d, del = 0x%03x, dur = 0x%02x\n",chn,del,dur);

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIBgoFifoEmpty : public MenuItem {
public:
  TTCVIBgoFifoEmpty() { setName("check if BGO FIFO empty"); }
  int action() {
    bool	empty;

    int chn = enterInt("BGO channel",0,TTCVI::BGO_NUM-1);
    if((rtnv = ttcvi->bgoFifoEmpty(chn,&empty)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVIBgoFifoEmpty: BGO FIFO empty = " << empty << std::endl;

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIBgoFifoFull : public MenuItem {
public:
  TTCVIBgoFifoFull() { setName("check if BGO FIFO full"); }
  int action() {
    bool	full;

    int chn = enterInt("BGO channel",0,TTCVI::BGO_NUM-1);
    if((rtnv = ttcvi->bgoFifoFull(chn,&full)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVIBgoFifoFull: BGO FIFO full = " << full << std::endl;

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIBgoFifoRetransSet : public MenuItem {
public:
  TTCVIBgoFifoRetransSet() { setName("set BGO FIFO retransmit flag"); }
  int action() {

    int chn = enterInt("BGO channel",0,TTCVI::BGO_NUM-1);
    int flg = enterInt("retransmit ",0,1);
    if((rtnv = ttcvi->bgoFifoRetransSet(chn,(flg>0))) != 0) {
      return(rtnv);
    }

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIBgoFifoRetransGet : public MenuItem {
public:
  TTCVIBgoFifoRetransGet() { setName("get BGO FIFO retransmit flag"); }
  int action() {
    bool	flg;

    int chn = enterInt("BGO channel",0,TTCVI::BGO_NUM-1);
    if((rtnv = ttcvi->bgoFifoRetransGet(chn,&flg)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVIBgoFifoRetranmsitGet: BGO FIFO retransmit flag = " << flg << std::endl;

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIBgoFifoReset : public MenuItem {
public:
  TTCVIBgoFifoReset() { setName("reset BGO FIFO"); }
  int action() {

    int chn = enterInt("BGO channel",0,TTCVI::BGO_NUM-1);
    return(ttcvi->bgoFifoReset(chn));
  }
};

//------------------------------------------------------------------------------

class TTCVIAsyncCommand : public MenuItem {
public:
  TTCVIAsyncCommand() { setName("send asynchronous command"); }
  int action() {

    int sht = enterInt("short      ",0,1);

    if(sht) {
      TTCVI::ShortCommand cmd =
	(TTCVI::ShortCommand) enterHex("command",0x00,0xff);
      rtnv = ttcvi->asyncCommand(cmd);
    }
    else {
      TTCVI::LongCommand	cmd;
      cmd.address = enterHex("address    ",0x0000,0x3fff);
      cmd.external = bool(enterInt("external   ",0,1));
      cmd.sub_address = (u_char)enterHex("sub-address",0x00,0xff);
      cmd.data = (u_char)enterHex("data       ",0x00,0xff);
      rtnv = ttcvi->asyncCommand(cmd);
    }

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIAsyncPendingGet : public MenuItem {
public:
  TTCVIAsyncPendingGet() { setName("get asynchronous command pending flag"); }
  int action() {
    bool	flg;

    if((rtnv = ttcvi->asyncPendingGet(&flg)) != 0) {
      return(rtnv);
    }
    std::cout << "TTCVIAsyncPendignGet: asynchronous command pending flag = " << flg << std::endl;

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVICTPSlave : public MenuItem {
public:
  TTCVICTPSlave() { setName("typical CTP Slave setup"); }
  int action() {
    rtnv = 0;

    int BCR_delay = enterInt("Enter BCR delay", 0, 3511);
    int useTriggerWord = enterInt("Use trigger word? (0=no, 1=yes)", 0, 1);
    int ext(0);
    if (useTriggerWord==1) {
      ext = enterInt("0=internal, 1=external", 0, 1);
    }

    rtnv |= ttcvi->reset(true);
    rtnv |= ttcvi->l1aInputSet(TTCVI::L1A_EXT0);
    rtnv |= ttcvi->counterSelectionSet(TTCVI::CNT_L1A);
    rtnv |= ttcvi->orbitInputSet(TTCVI::ORB_EXT);

    // retransmit
    rtnv |= ttcvi->bgoFifoRetransSet(0, true);
    rtnv |= ttcvi->bgoFifoRetransSet(1, true);
    rtnv |= ttcvi->bgoFifoRetransSet(2, false);
    rtnv |= ttcvi->bgoFifoRetransSet(3, false);

    RCD::TTCVI::ShortCommand bcr_command(1);
    rtnv |= ttcvi->bgoCommandPut(0, bcr_command); // short command 1 for BCR
    rtnv |= ttcvi->bgoModeSet(0, TTCVI::BGO_SYNC);
    // BGo Inhibit on: channel, delay, duration
    rtnv |= ttcvi->bgoInhibitOn(0, static_cast<u_short>(BCR_delay), 51);

    // setup ECR
    RCD::TTCVI::ShortCommand ecr_command(2);
    rtnv |= ttcvi->bgoCommandPut(1, ecr_command); // short command 2 for ECR
    rtnv |= ttcvi->bgoModeSet(1, TTCVI::BGO_ENABLE+TTCVI::BGO_SINGLE);
    // BGo Inhibit on: channel, delay, duration
    rtnv |= ttcvi->bgoInhibitOn(1, 0, 1);

    if (useTriggerWord==1) {
      // setup trigger word (broadcast: rx address = 0)
      TTCVI::LongCommand lc;
      lc.address = 0;
      lc.external = ext; // only internal rx registers
      lc.sub_address = 0;
      lc.data = 0; // not used
      rtnv |= ttcvi->triggerWordEnable(lc);
    } else {
      rtnv |= ttcvi->triggerWordDisable();
    }
      
    ttcvi->counterReset();

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIBCR : public MenuItem {
public:
  TTCVIBCR() { setName("setup BCR"); }
  int action() {
    rtnv = 0;

    int BCR_delay = enterInt("Enter BCR delay", 0, 3511);

    // retransmit
    rtnv |= ttcvi->bgoFifoRetransSet(0, true);

    RCD::TTCVI::ShortCommand bcr_command(1);
    rtnv |= ttcvi->bgoCommandPut(0, bcr_command); // short command 1 for BCR
    rtnv |= ttcvi->bgoModeSet(0, TTCVI::BGO_SYNC);
    // BGo Inhibit on: channel, delay, duration
    rtnv |= ttcvi->bgoInhibitOn(0, static_cast<u_short>(BCR_delay), 51);

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIECR : public MenuItem {
public:
  TTCVIECR() { setName("setup ECR"); }
  int action() {
    rtnv = 0;

    // setup ECR
    rtnv |= ttcvi->bgoFifoRetransSet(1, true);
    RCD::TTCVI::ShortCommand ecr_command(2);
    rtnv |= ttcvi->bgoCommandPut(1, ecr_command); // short command 2 for ECR
    rtnv |= ttcvi->bgoModeSet(1, TTCVI::BGO_ENABLE+TTCVI::BGO_SINGLE);
    // BGo Inhibit on: channel, delay, duration
    rtnv |= ttcvi->bgoInhibitOn(1, 0, 1);

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class TTCVIECRL1A : public MenuItem {
public:
  TTCVIECRL1A() { setName("play ECR/L1A sequence"); }
  int action() {
    rtnv = 0;

    std::cout << "Send a sequence of 1 ECR (incl. 1ms deadtime before and after) followed by 1 L1A" << std::endl;

    int n = enterInt("Number of consecutive sequences ", 0, 1000000);

    // remember old trigger and BGo-1 settings
    u_short old_l1a(0), old_bgo1(0);
    rtnv |= ttcvi->l1aInputGet(&old_l1a);
    rtnv |= ttcvi->bgoModeGet(1, &old_bgo1);

    // switch trigger source to VME
    rtnv |= ttcvi->l1aInputSet(TTCVI::L1A_VME);

    // switch off BGo-1
    rtnv |= ttcvi->bgoModeSet(1, TTCVI::BGO_SINGLE);

    // initialise timer
    int ret = ts_open(1, TS_DUMMY);
    if (ret) rcc_error_print(stdout, ret);
    const u_int t_1ms(1000); // in microseconds

    RCD::TTCVI::ShortCommand ecr_command(2);
    for (int i = 0; i < n; ++i) {

      // wait 1ms
      ret = ts_delay(t_1ms);
      if (ret) rcc_error_print(stdout, ret);

      // issue one ECR
      rtnv |= ttcvi->asyncCommand(ecr_command);

      // wait 1ms
      ret = ts_delay(t_1ms);
      if (ret) rcc_error_print(stdout, ret);

      // issue one trigger
      rtnv |= ttcvi->l1aGenerate();

    }

    // restore old trigger and BGo-1 settings
    rtnv |= ttcvi->l1aInputSet(old_l1a);
    rtnv |= ttcvi->bgoModeSet(1, old_bgo1);

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

int main(int argc, char** argv) {

  CmdArgStr  base_cmd('a', "address", "vme-base-address",  "VME base address in hex", CmdArg::isOPT);
  base_cmd = "";

  CmdLine  cmd(*argv, &base_cmd, nullptr);
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.description("Command-line driven program to communicate with a TTCvi board");
  cmd.parse(arg_iter);

  if (base_cmd=="") {
    // no argument given, ask for base address
    std::string in;
    std::cout << "<base>?" << std::endl;
    getline(cin,in);
    if(sscanf(in.c_str(),"%x",&base) == EOF) exit(EINVAL);
    if(base < 0x100) base = base << 16;
  } else {
    // get first argument, the base address
    std::string in(base_cmd);
    sscanf(argv[1], "%x", &base);
    if(base < 0x100) base = base << 16;
  }

  printf(" VME base adresss = 0x%06x\n", base);

  Menu	menu("TTCVI main menu");

  // generate menu
  menu.init(new TTCVIOpen);
  menu.add(new TTCVIReset);
  menu.add(new TTCVIDump);
  menu.exit(new TTCVIClose);

  Menu	menuGEN("GEN menu");
  menuGEN.add(new TTCVIGENBcDelayGet);
  menuGEN.add(new TTCVIGENOrbitInputSet);
  menuGEN.add(new TTCVIGENOrbitInputGet);
  menuGEN.add(new TTCVITrigWordEnable);
  menuGEN.add(new TTCVITrigWordDisable);
  menuGEN.add(new TTCVITrigWordGet);
  menuGEN.add(new TTCVICounterGet);
  menuGEN.add(new TTCVICounterSelectionSet);
  menuGEN.add(new TTCVICounterSelectionGet);
  menuGEN.add(new TTCVICounterReset);
  menu.add(&menuGEN);

  Menu	menuL1A("L1A menu");
  menuL1A.add(new TTCVIL1aInputSet);
  menuL1A.add(new TTCVIL1aInputGet);
  menuL1A.add(new TTCVIL1aRandomSet);
  menuL1A.add(new TTCVIL1aRandomGet);
  menuL1A.add(new TTCVIL1aGenerate);
  menuL1A.add(new TTCVIL1aFifoEmpty);
  menuL1A.add(new TTCVIL1aFifoFull);
  menuL1A.add(new TTCVIL1aFifoReset);
  menu.add(&menuL1A);

  Menu	menuBGO("BGO menu");
  menuBGO.add(new TTCVIBgoModeSet);
  menuBGO.add(new TTCVIBgoModeGet);
  menuBGO.add(new TTCVIBgoCommandPut);
  menuBGO.add(new TTCVIBgoGenerate);
  menuBGO.add(new TTCVIBgoInhibitOn);
  menuBGO.add(new TTCVIBgoInhibitOff);
  menuBGO.add(new TTCVIBgoInhibitGet);
  menuBGO.add(new TTCVIBgoFifoEmpty);
  menuBGO.add(new TTCVIBgoFifoFull);
  menuBGO.add(new TTCVIBgoFifoRetransSet);
  menuBGO.add(new TTCVIBgoFifoRetransGet);
  menuBGO.add(new TTCVIBgoFifoReset);
  menuBGO.add(new TTCVIAsyncCommand);
  menuBGO.add(new TTCVIAsyncPendingGet);
  menu.add(&menuBGO);

  Menu	menuHigh("Higher Level Functions");
  menuHigh.add(new TTCVICTPSlave);
  menuHigh.add(new TTCVIBCR);
  menuHigh.add(new TTCVIECR);
  menuHigh.add(new TTCVIECRL1A);
  menu.add(&menuHigh);

  // start menu
  menu.execute();
}
