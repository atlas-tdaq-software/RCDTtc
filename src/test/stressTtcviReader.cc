//******************************************************************************
  // file: stressTtcviReader.cc
  // desc: test program that reads continously from VME register to test for
  //     glitches and cross talk
  //******************************************************************************

#include <fstream>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <list>
#include <errno.h>
#include <utility>

#include <string>
#include <vector>
#include "RCDVme/RCDVme.h"
#include "cmdl/cmdargs.h"

using namespace RCD;
using namespace std;

int main(int argc, char** argv) {

  CmdArgBool verbose('v', "verbose", "verbose mode", CmdArg::isOPT);
  CmdArgBool random('r', "random", "random access", CmdArg::isOPT);
  CmdArgStr registerfile('f', "register-file", "register-file", "file that contains registers", CmdArg::isOPT);
  CmdArgInt address('a', "address", "address", "VME base address", CmdArg::isOPT);

  verbose = false;
  random = false;
  registerfile = "ttcvi-registers.dat";
  address = 0xff1000;

  CmdLine  cmd(	*argv, &verbose, &random, &registerfile, &address, nullptr);

  cmd.description("This program reads randomly readable VME addresses of the TTCvi for test purposes");

  CmdArgvIter	argvIter(--argc, ++argv);
  unsigned status = cmd.parse(argvIter);
  if (status) {
    std::cerr << "Parsing of command line parameters failed!" << std::endl;
    exit(1);
  }

  std::cout << "verbose = " << verbose << std::endl;
  std::cout << "random = " << random << std::endl;
  std::cout << "registerfile = " << registerfile << std::endl;
  std::cout << "address = 0x" << std::hex << address << std::dec << std::endl;

  int stat = 0;

  u_short data;
  u_int areg;

  // fill vector from file:
  std::vector<u_short> regs;
  char buffer[100];

  std::ifstream tfile(registerfile, ios::in);
  if (!tfile.is_open()) {
    return(-1);
  }
  else {
    std::cout << "Loading file \"" << registerfile << "\" into memory" << std::endl;
    while(!tfile.eof()){
      tfile.getline(buffer,100);
      if (strlen(buffer)>2) {
 	sscanf(buffer,"%x",&areg);
	regs.push_back(areg);
      }
    }
  }
  
  std::cout << "Read " << regs.size() << " registers from file: ";
  for (std::vector<u_short>::const_iterator it = regs.begin();
       it != regs.end();
       ++it) {
    std::cout << std::hex << " 0x" << *it << std::dec;
  }
  std::cout << std::endl;

  int nreg = regs.size();

  // VME
  static const u_int VME_SIZE = 0x0100;
  VME* m_vme = VME::Open();
  VMEMasterMap* m_vmm = m_vme->MasterMap(address,VME_SIZE,VME_A24,0);

  if ((*m_vmm)()) {
    std::cout << "Could not open VME master map at base 0x" << std::hex << address << std::dec << std::endl;
    exit(-1);
  } else {
    std::cout << "Opening VME master map at base 0x" << std::hex << address << std::dec << std::endl;
  }

  // initialise random seed with time
  srand( time(nullptr) );
  
  u_short r(0);
  u_int reg(0);

  if (random) {
    while(true) {
      // get random value between 0 and nreg-1
      r = rand() % (nreg-1);
      reg = regs[r];

      if ( (stat = m_vmm->ReadSafe(reg,&data)) != VME_SUCCESS) {
	std::cout << "VME error while reading from register[" << std::dec << r << "] = 0x" << std::hex << reg << std::dec << std::endl;
	exit(stat);
      }
    
      if (verbose) {
	std::cout << "Read 0x" << std::hex << data << " from register[" << std::dec << r << "] = 0x" << std::hex << reg << std::dec << std::endl;
      }
    }
  } else {

    while (true) {
      for (int i = 0; i < nreg; ++i) {
	reg = regs[i];
	int niter = 200000;
	std::cout << "Accessing register[" << std::dec << i << "] = 0x" << std::hex << reg << std::dec << "  " << niter << " times" << std::endl;
	for (int j=0; j<niter; ++j) {
	  if ( (stat = m_vmm->ReadSafe(reg,&data)) != VME_SUCCESS) {
	    std::cout << "VME error while reading from register[" << std::dec << r << "] = 0x" << std::hex << reg << std::dec << std::endl;
	    exit(stat);
	  }
	}
      }
    }
  }

  return 0;
}
